﻿
using System.Collections.Generic;
using System.Linq;
using Robot.Common;

namespace RomanRobot
{
    using Droid = Robot.Common.Robot;

    public class WarMachine
    {
        
        public static Droid IsThereEnemy(Droid myRobot, IList<Droid> robots, Position target, int range)
        {
            var enemys = robots.Where(t => !t.Owner.Equals(myRobot.Owner)).ToList();
            List<Droid> threats = new List<Droid>();
            foreach (var enemy in enemys)
            {
                if (IsInArea(enemy, target, range))
                {
                    threats.Add(enemy);
                }
            }
            
            if (threats.Count != 0)
            {
                threats.Sort(
                    (robot, robot1) => DistanceHelper.FindDistance(target, robot.Position) >=
                                       DistanceHelper.FindDistance(target, robot1.Position)
                        ? 1
                        : -1);
                Droid enemy = threats[0];
                foreach (var threat in threats)
                {
                    if (threat.Energy > enemy.Energy)
                    {
                        enemy = threat;
                    }
                }

                return enemy;
            }
            return null;
        }

        public static int AttackPotential(Droid myRobot, Droid enemyRobot)
        {
            int energyLoss = DistanceHelper.FindDistance(myRobot.Position, enemyRobot.Position) + 30;
            int energyGain = enemyRobot.Energy / 10;
            return energyGain - energyLoss;
        }

        public static bool IsInArea(Droid robot, Position area, int range)
        {
            int MinX = area.X - range > 0 ? area.X - range : 0;
            int MinY = area.Y - range > 0 ? area.Y - range : 0;
            int MaxX = area.X + range < 100 ? area.X +range : 99;
            int MaxY = area.Y + range < 100 ? area.Y + range : 99;
            if (robot.Position.X >= MinX &&
                robot.Position.X <= MaxX &&
                robot.Position.Y >= MinY &&
                robot.Position.Y <= MaxY)
                return true;
            return false;
        }
    }
}
