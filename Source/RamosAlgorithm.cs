﻿
using System.Collections.Generic;
using Robot.Common;


namespace RomanRobot
{
    public class RamosAlgorithm : IRobotAlgorithm
    {

        public PositionManager Manager = new PositionManager();
        public int myRobotsCount = 10;
        public int RoundCount { get; set; }

        public RamosAlgorithm()
        {
            Logger.OnLogRound += Logger_OnLogRound;
        }

        void Logger_OnLogRound(object sender, LogRoundEventArgs e)
        {
            RoundCount++;
        }

        public string Author
        {
            get { return "Roman"; }
        }

        public string Description
        {
            get { return "Demo for students"; }
        }

        public RobotCommand DoStep(IList<Robot.Common.Robot> robots, int robotToMoveIndex, Map map)
        {
            var myRobot = robots[robotToMoveIndex];

            if(!Manager.isSet)
                Manager.Setup(map);

            Position target = Manager.IsBounded(robotToMoveIndex);
            if (target == null)
            {
                target = Manager.Bound(myRobot, robotToMoveIndex);
            }
            if(myRobot.Position != target)
            {              
                if(myRobot.Energy <=4)
                    return new CollectEnergyCommand();
                return new MoveCommand(){ NewPosition = Manager.OptimalMove(myRobot, target) };
            }
            else
            {
                Robot.Common.Robot threat = WarMachine.IsThereEnemy(myRobot, robots, myRobot.Position, 3);
                if (threat != null)
                {
                    if(WarMachine.AttackPotential(myRobot,threat) > Manager.PossibleResource(map, myRobot.Position)/2 && 
                       DistanceHelper.FindDistance(myRobot.Position, threat.Position) + 30 < myRobot.Energy)
                        return new MoveCommand(){ NewPosition = threat.Position};
                }

                if (Manager.PossibleResource(map, myRobot.Position) == 0)
                {
                    threat = WarMachine.IsThereEnemy(myRobot, robots, myRobot.Position, 5);
                    
                    if(threat!=null && WarMachine.AttackPotential(myRobot,threat) > 0 && DistanceHelper.FindDistance(myRobot.Position, threat.Position) + 30 < myRobot.Energy)
                        return new MoveCommand() { NewPosition = threat.Position };
                }

                if (myRobot.Energy > 300 && Manager.MaxCount > myRobotsCount && RoundCount < 40 &&
                    Manager.PossibleResource(map, myRobot.Position) < 200)
                {
                    myRobotsCount++;
                    return new CreateNewRobotCommand() { NewRobotEnergy = (myRobot.Energy - 100) > 250 ? 100 : (int)((myRobot.Energy - 100)* 0.8) };
                }
                    
                return new CollectEnergyCommand();
            }
        }    
    }   
}
