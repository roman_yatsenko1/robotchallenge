﻿using System;
using System.Collections.Generic;
using Robot.Common;


namespace RomanRobot
{
    using Droid = Robot.Common.Robot;
    public class PositionManager
    {
        private Dictionary<int, Position> connection = new Dictionary<int, Position>();
        private IList<Position> positions = new List<Position>();
        public int MaxCount = 0;
        public bool isSet = false;

        
        public Position Bound(Droid myRobot, int index)
        {
            Position closePosition = positions[0];
            foreach (var pos in positions)
            {
                if (DistanceHelper.FindDistance(myRobot.Position, closePosition) > DistanceHelper.FindDistance(myRobot.Position, pos))
                {
                    closePosition = pos;
                }
            }
            connection.Add(index, closePosition);
            positions.Remove(closePosition);
            return closePosition;
        }

        public Position IsBounded(int index)
        {
            return connection.TryGetValue(index, out Position pos) ? pos : null;
        }

        public void Setup(Map map)
        {
            IList<EnergyStation> stations = map.Stations;
            foreach (var station in stations)
            {
                Position pos = OptimalPosition(map, station.Position);
                if(positions.Contains(pos) == false)
                     positions.Add(pos);
            }

            MaxCount = positions.Count;
            isSet = true;
        }

        public Position OptimalPosition(Map map, Position area)
        {
            int MinX = area.X - 2 > 0 ? area.X - 2 : 0;
            int MinY = area.Y - 2 > 0 ? area.Y - 2 : 0;
            int MaxX = area.X + 2 < 100 ? area.X + 2 : 99;
            int MaxY = area.Y + 2 < 100 ? area.Y + 2 : 99;
            Position best = area;
            for (int i = MinX; i <=MaxX; i++)
            {
                for (int j = MinY; j <=MaxY; j++)
                {
                    if (CalculatePotential(map, new Position(i, j)) > CalculatePotential(map, best))
                    {
                        best = new Position(i, j);
                    }
                }
            }
            return best;
        }

        public int CalculatePotential(Map map, Position place)
        {
            int sum = 0;
            foreach (var station in map.GetNearbyResources(place, 2))
            {
                sum += station.RecoveryRate;
            }

            return sum;
        }

        public int PossibleResource(Map map, Position pos)
        {
            int sum = 0;
            foreach (var station in map.GetNearbyResources(pos, 2))
            {
                sum += station.Energy;
            }

            return sum;
        }

        public Position OptimalMove(Droid myRobot, Position target)
        {
            Position start = myRobot.Position;
            int energyLoss = DistanceHelper.FindDistance(start, target);
            if (myRobot.Energy / 3 > energyLoss)
            {
                return target;
            }
            else
            {
                if (myRobot.Energy < 12)
                {
                    int x = start.X;
                    int y = start.Y;
                    if (target.X != x)
                    {
                        x = target.X > x ? x + 1 : x - 1;
                    }

                    if (target.Y != y)
                    {
                        y = target.Y > y ? y + 1 : y - 1;
                    }
                    return new Position(x,y);
                }
                Position dest = new Position(myRobot.Position.X + ((target.X - myRobot.Position.X) / 2),
                    myRobot.Position.Y + ((target.Y - myRobot.Position.Y) / 2));
                Position second = OptimalMove(myRobot, dest);
                return second;
            }
        }
    }





}
